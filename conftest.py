import pytest

def pytest_addoption(parser):
    parser.addoption("--fast")

@pytest.fixture
def fast(request):
    return request.config.getoption("--fast")

